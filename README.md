# readme-template

Descrição de um parágrafo (algo próximo de 2 a 5 linhas).

> As palavras-chave "DEVE", "NÃO DEVE", "REQUER", "DEVERIA", "NÃO DEVERIA", "PODERIA", "NÃO PODERIA", "RECOMENDÁVEL", "PODE", e "OPCIONAL" neste documento devem ser interpretadas como descritas no [RFC 2119](http://tools.ietf.org/html/rfc2119). Tradução livre [RFC 2119 pt-br](http://rfc.pt.webiwg.org/rfc2119).

## Pré-requisito

Este tópico DEVE conter uma lista pontuando as dependências de primeira ordem requerida pela aplicação. Exemplo:

- NODE >= 14
- MySQL >= 5.7 <= 5.9

> Quando todas as dependências forem satisfeitas por uma solução como `docker-compose.yml` ou semelhante, este tópico DEVE tão somente referênciar a tecnologia base que encapsula as demais. Exemplo:

- Docker Engine >= 18.09

## Como executar

Este tópico deve conter o passo-a-passo necessário para obter o código fonte, instalar as dependências e executar o projeto. Ex:

Para clonar o projeto:
```
git clone https://...
```

Para instalar as dependências:
```
```

